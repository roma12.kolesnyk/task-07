package com.epam.rd.java.basic.task7.db;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {
    public static final String URL = "jdbc:derby:memory:testdb;create=true";
    public static final String INSERT_USER_BY_LOGIN = "insert into users values(default, ?)";
    public static final String SELECT_ALL_USERS = "select * from users";
    public static final String INSERT_TEAM_BY_NAME = "insert into teams values(default, ?)";
    public static final String SELECT_ALL_TEAMS = "select * from teams";
    public static final String INSERT_USER_TEAMS = "insert into users_teams values(?, ?)";
    public static final String GET_TEAMS_BY_USER_ID = "select t.id, t.name " +
                    "from users_teams " +
                    "join teams t on t.id = users_teams.team_id " +
                    "where user_id = ?";
    public static final String DELETE_TEAM_BY_NAME = "delete from teams where name = ?";
    public static final String DELETE_USER_TEAMS = "delete from users_teams where team_id not in (select team_id from teams)";
    public static final String DELETE_USER_BY_LOGIN = "delete from users where login = ?";
    public static final String UPDATE_TEAM_NAME_BY_ID = "update teams set name = ? where id = ?";

    private static DBManager instance;

    public static synchronized DBManager getInstance() {
        if (instance == null) instance = new DBManager();
        return instance;
    }

    private DBManager() {
    }

    public List<User> findAllUsers() throws DBException {
        try (Connection con = DriverManager.getConnection(URL);
             PreparedStatement ps = con.prepareStatement(SELECT_ALL_USERS, Statement.RETURN_GENERATED_KEYS)) {

            ResultSet rs = ps.executeQuery();

            List<User> users = new ArrayList<>();
            while (rs.next()) {
                users.add(new User(rs.getInt(1), rs.getString(2)));
            }
            return users;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException(e.getMessage(), e);
        }
    }

    public boolean insertUser(User user) throws DBException {
        if (findAllUsers().contains(user)) return false;

        try (Connection con = DriverManager.getConnection(URL);
             PreparedStatement ps = con.prepareStatement(INSERT_USER_BY_LOGIN, Statement.RETURN_GENERATED_KEYS)) {

            ps.setString(1, user.getLogin());
            ps.execute();

            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                user.setId(rs.getInt(1));
            }

            System.out.println("user was added");
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException(e.getMessage(), e);
        }
    }

    public static void main(String[] args) throws Exception{
        DBManager dbManager = DBManager.getInstance();
        dbManager.deleteUsers(User.createUser("user0"));

    }

    public boolean deleteUsers(User... users) throws DBException {
        try (Connection con = DriverManager.getConnection(URL);
             PreparedStatement ps = con.prepareStatement(DELETE_USER_BY_LOGIN, Statement.RETURN_GENERATED_KEYS)) {

            for (User user : users) {
                ps.setString(1, user.getLogin());
                ps.executeUpdate();
            }

            System.out.println("users were deleted");
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException(e.getMessage(), e);
        }
    }

    public User getUser(String login) throws DBException {
        return findAllUsers().stream()
                .filter(u -> login.equals(u.getLogin()))
                .findFirst()
                .orElseThrow();
    }

    public Team getTeam(String name) throws DBException {
        return findAllTeams().stream()
                .filter(t -> name.equals(t.getName()))
                .findFirst()
                .orElseThrow();
    }

    public List<Team> findAllTeams() throws DBException {
        try (Connection con = DriverManager.getConnection(URL);
             PreparedStatement ps = con.prepareStatement(SELECT_ALL_TEAMS, Statement.RETURN_GENERATED_KEYS)) {

            ResultSet rs = ps.executeQuery();

            List<Team> teams = new ArrayList<>();
            while (rs.next()) {
                teams.add(new Team(rs.getInt(1), rs.getString(2)));
            }
            rs.close();
            return teams;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException(e.getMessage(), e);
        }
    }

    public boolean insertTeam(Team team) throws DBException {
        try (Connection con = DriverManager.getConnection(URL);
             PreparedStatement ps = con.prepareStatement(INSERT_TEAM_BY_NAME, Statement.RETURN_GENERATED_KEYS)) {

            ps.setString(1, team.getName());
            ps.executeUpdate();

            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                team.setId(rs.getInt(1));
            }

            System.out.println("team was added");
            rs.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException(e.getMessage(), e);
        }
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        // if in user_teams is user.login then delete him and add again
        // якщо хоча б одна команда вже була закріплена за цим коритсувачем то кидати DBException
        try (Connection con = DriverManager.getConnection(URL);
             PreparedStatement ps = con.prepareStatement(INSERT_USER_TEAMS)) {

            con.setAutoCommit(false);
            List<Team> currentUserTeams = getUserTeams(user);

            try {
                for (Team team : teams) {
                    ps.setInt(1, user.getId());
                    ps.setInt(2, team.getId());

                    if (currentUserTeams.contains(team)) throw new SQLException(user.getLogin() + " already in this team");

                    ps.executeUpdate();
                }
                con.commit();
                System.out.println("All teams was added for user");
            } catch (SQLException e) {
                System.out.println("None of teams was added for user because of error");
                System.err.println(e.getMessage());
                con.rollback();
                throw new SQLException(e);
            }
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        return true;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        try (Connection con = DriverManager.getConnection(URL);
             PreparedStatement ps = con.prepareStatement(GET_TEAMS_BY_USER_ID, Statement.RETURN_GENERATED_KEYS)) {

            ps.setInt(1, user.getId());
            ResultSet rs = ps.executeQuery();

            List<Team> teams = new ArrayList<>();
            while (rs.next()) {
                teams.add(new Team(rs.getInt(1), rs.getString(2)));
            }
            return teams;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException(e.getMessage(), e);
        }
    }

    public boolean deleteTeam(Team team) throws DBException {
        try (Connection con = DriverManager.getConnection(URL);
             PreparedStatement ps = con.prepareStatement(DELETE_TEAM_BY_NAME)) {

            ps.setString(1, team.getName());
            ps.executeUpdate();

            deleteUserTeams(team);

            System.out.println("team was deleted");
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException(e.getMessage(), e);
        }
    }

    private void deleteUserTeams(Team team) throws DBException {
        try (Connection con = DriverManager.getConnection(URL);
             PreparedStatement ps = con.prepareStatement(DELETE_USER_TEAMS)) {

            ps.execute();

            System.out.println("team was deleted");
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException(e.getMessage(), e);
        }
    }

    public boolean updateTeam(Team team) throws DBException {
        try (Connection con = DriverManager.getConnection(URL);
             PreparedStatement ps = con.prepareStatement(UPDATE_TEAM_NAME_BY_ID)) {

            ps.setString(1, team.getName());
            ps.setInt(2, team.getId());
            ps.executeUpdate();

            System.out.println("team was updated");
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException(e.getMessage(), e);
        }
    }

}
